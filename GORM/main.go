package main

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)


type TestCol struct{
	ANumber int `gorm:"primary_key"`
	AName string `gorm:"type:text" gorm:"column:'a_name'"`
	AnAddress string `gorm:"type:text" gorm:"column:'an_address'"`
	AUint uint `gorm:"type:uint"`
}
type SymptomStatsGORM struct {
	MarketShortKey             string `gorm:"primary_key"`
	SymptomID                  string `gorm:"primary_key"`
	ServiceType                string
	CheckFirstFix1             string
	CheckFirstFix2             string
	MinRangeCycleTime          int
	CycleAverageDays           uint32
	MaxRangeCycleTime          uint32
	FirstVisitRepairLikelihood uint32
	RepairLikelihood1yr        uint32
	RepairLikelihood1To5yr     uint32
	RepairLikelihood6To10yr    uint32
	RepairLikelihood11To15yr   uint32
	RepairLikelihood16Plus     uint32
	RepairLikelihoodUnknown    uint32
	DIYLink                    string
	RepairCostMin              uint32
	RepairCostMax              uint32
	ReplaceCostMin             uint32
	ReplaceCostMax             uint32
}

type DevOnDemandCrystalBallSymptomStats struct {
	MarketShortKey             string `gorm:"varchar(10)" gorm:"primary_key"`
	SymptomID                  string `gorm:"primary_key" gorm:"varchar(40)"`
	ServiceType                string `gorm:"type:text" gorm:"column:'Service Type'"`
	CheckFirstFix1             string `gorm:"type:text" gorm:"column:'Check First Fix 1'"`
	CheckFirstFix2             string `gorm:"type:text" gorm:"column:'Check First Fix 2'"`
	MinRangeCycleTime          int `gorm:"type:int" gorm:"column:'Min Range Cycle Time (days)'"`
	CycleAverageDays           int `gorm:"type:int" gorm:"column:'Avg Range Cycle Time (Days)'"`
	MaxRangeCycleTime          uint `gorm:"type:int" gorm:"column:'Max Range Cycle Time (days)'"`
	FirstVisitRepairLikelihood uint `gorm:"type:int" gorm:"column:'First Visit Repair Likelihood (%)'"`
	RepairLikelihood1yr        uint `gorm:"type:uint" gorm:"column:'Repair Likelihood: <1 Yr' "`
	RepairLikelihood1To5yr     uint32 `gorm:"type:uint" gorm:"column:'Repair Likelihood: 1-5 Yr'"`
	RepairLikelihood6To10yr    uint32 `gorm:"type:int" gorm:"column:'Repair Likelihood: 6-10 Yr'"`
	RepairLikelihood11To15yr   uint32 `gorm:"type:int" gorm:"column:'Repair Likelihood: 11-15 Yr'"`
	RepairLikelihood16Plus     uint32 `gorm:"type:int" gorm:"column:'Repair Likelihood: 16+ Yr'"`
	RepairLikelihoodUnknown    uint32 `gorm:"type:int" gorm:"column:'Repair Likelihood: Unknown'"`
	DIYLink                    string `gorm:"type:text" gorm:"column:'DIY Link(s) 1'"`
	RepairCostMin              uint32 `gorm:"type:int" gorm:"column:'Repair Cost Min'"`
	RepairCostMax              uint32 `gorm:"type:int" gorm:"column:'Repair Cost Max'"`
	ReplaceCostMin             uint32 `gorm:"type:int" gorm:"column:'Replace Cost Min'"`
	ReplaceCostMax             uint32 `gorm:"type:int" gorm:"column:'Replace Cost Max'"`
}


type DbConfig struct {
	DbName     string `required:"true" split_words:"true" default:"crystal-ball"`
	DbUser     string `required:"true" split_words:"true" default:"root"`
	DbPassword string `required:"true" split_words:"true" default:"AYEbhat84!!"`
	DBAddress  string `required:"true" split_words:"true" default:"127.0.0.1:3306"`
	TblName string `required:"true" split_words:"true" default:"dev_on_demand_crystal_ball_symptom_stats"`
}

func main() {
	conf := DbConfig{
		"crystal-ball", "root", "AYEbhat84!!", "127.0.0.1:3306", "dev_on_demand_crystal_ball_symptom_stats",
	}
	dsn := conf.DbUser+":"+conf.DbPassword+"@tcp("+conf.DBAddress+")/"+conf.DbName

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	if err!=nil{
		log.Println("Connection Failed to Open")
	}
	log.Println("Connection Established")

	//Auto create table based on Model
	db.Debug().AutoMigrate(&SymptomStatsGORM{})

db.Debug().AutoMigrate(&TestCol{})

	db.Delete(&TestCol{}, []int{11,22,33})
	// DELETE FROM test_col WHERE ANumber IN (1,2,3);

	var test_cols = []TestCol{
		{AName: "Name1", ANumber: 11, AnAddress: "41414 Denver CO", AUint:34},
		{AName: "Name2", ANumber: 22, AnAddress: "Lions Loop, Idaho Springs CO", AUint:44},
		{AName: "Name3", ANumber:33, AnAddress:"Writers Row, Glenwood Springs CO", AUint:66},
		{AName: "Name4", ANumber: 77, AnAddress: "15151 Arapahoe Rd., Denver CO", AUint:77},
		{AName: "Name5", ANumber: 88, AnAddress:"Bellevue, Greenwood Village CO", AUint:88},
	}
	db.Create(&test_cols)



	//Batch delete
	//db.Where("a_name LIKE ?", "%Name%").Delete(TestCol{})

	symp := DevOnDemandCrystalBallSymptomStats{}
	db.Where(&DevOnDemandCrystalBallSymptomStats{MarketShortKey: "DEN", SymptomID: "68802fda-3f1b-444f-8428-adae92634af5"}).Find(&symp)
	//db.Table("dev_on_demand_crystal_ball_symptom_stats").Find(&symp, &DevOnDemandCrystalBallSymptomStats{MarketShortKey: "ATL"})
	db.Table("dev_on_demand_crystal_ball_symptom_stats").Where(&DevOnDemandCrystalBallSymptomStats{MarketShortKey: "DEN"}).Find(&symp)
	log.Println(symp)



	rr := TestCol{}
	db.Where(&TestCol{ ANumber: 22}).Find(&rr)
	log.Println(rr)

	// SELECT * FROM users LIMIT 1;
	/*
	db.Where("address = ?", "Los Angeles").Find(&user)
	//SELECT * FROM user_models WHERE address=’Los Angeles’;

	db.Where("address <> ?", "New York").Find(&user)
	//SELECT * FROM user_models WHERE address<>’Los Angeles’;

	// IN
	db.Where("name in (?)", []string{"John", "Martin"}).Find(&user)

	// LIKE
	db.Where("name LIKE ?", "%ti%").Find(&user)

	// AND
	db.Where("name = ? AND address >= ?", "Martin", "Los Angeles").Find(&user)

	*/

}



