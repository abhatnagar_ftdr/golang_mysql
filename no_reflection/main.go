package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

type DbConfig struct {
	DbName     string `required:"true" split_words:"true" default:"crystal-ball"`
	DbUser     string `required:"true" split_words:"true" default:"root"`
	DbPassword string `required:"true" split_words:"true" default:"AYEbhat84!!"`
	DBAddress  string `required:"true" split_words:"true" default:"127.0.0.1:3306"`
	TblName    string `required:"true" split_words:"true" default:"dev_on_demand_crystal_ball_symptom_stats"`
}

type SymptomStats struct {
	MarketShortKey             string
	SymptomID                  string
	ServiceType                string
	CheckFirstFix1             string
	CheckFirstFix2             string
	MinRangeCycleTime          int
	CycleAverageDays           uint32
	MaxRangeCycleTime          uint32
	FirstVisitRepairLikelihood uint32
	RepairLikelihood1yr        uint32
	RepairLikelihood1To5yr     uint32
	RepairLikelihood6To10yr    uint32
	RepairLikelihood11To15yr   uint32
	RepairLikelihood16Plus     uint32
	RepairLikelihoodUnknown    uint32
	DIYLink                    string
	RepairCostMin              uint32
	RepairCostMax              uint32
	ReplaceCostMin             uint32
	ReplaceCostMax             uint32 
}

func main() {
	conf := DbConfig{
		"crystal-ball", "root", "AYEbhat84!!", "127.0.0.1:3306","dev_on_demand_crystal_ball_symptom_stats",
	}

	db, err := sql.Open("mysql", conf.DbUser+":"+conf.DbPassword+"@tcp("+conf.DBAddress+")/"+conf.DbName)
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Connected to the Db")
	}

	fmt.Println(conf.DbUser+":"+conf.DbPassword+"@tcp("+conf.DBAddress+")/"+conf.DbName)
	//defer db.Close()
	query := "SELECT * FROM "+conf.TblName+" WHERE symptom_id= '68802fda-3f1b-444f-8428-adae92634af5'  and market_short_key = 'DEN';"
	fmt.Println("our query: "+query)
	symptoms, err := db.Query(query)
	if err != nil {
		panic(err.Error())
	}
	err = db.Ping()
	if err != nil{
		fmt.Println("Ping failed")
	} else{
		fmt.Println("Ping succeeded")
	}

	for symptoms.Next() {
		var symStat SymptomStats
		err := symptoms.Scan(
			&symStat.SymptomID,
			&symStat.MarketShortKey,
			&symStat.ServiceType,
			&symStat.CheckFirstFix1,
			&symStat.CheckFirstFix2,
			&symStat.MinRangeCycleTime,
			&symStat.MaxRangeCycleTime,
			&symStat.CycleAverageDays,
			&symStat.FirstVisitRepairLikelihood,
			&symStat.RepairLikelihood1yr,
			&symStat.RepairLikelihood1To5yr,
			&symStat.RepairLikelihood6To10yr,
			&symStat.RepairLikelihood11To15yr,
			&symStat.RepairLikelihood16Plus,
			&symStat.RepairLikelihoodUnknown,
			&symStat.DIYLink,
			&symStat.RepairCostMin,
			&symStat.RepairCostMax,
			&symStat.ReplaceCostMin,
			&symStat.ReplaceCostMax)

		if err != nil {
			panic(err.Error())
		}
		fmt.Println(symStat)
	}

}
